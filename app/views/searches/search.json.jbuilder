json.users(@users) do |user|
  json.extract! user, :id, :facebook_id, :name, :email, :avatar_url, :password
  json.url user_url(user, format: :json)
  json.total_followed_users user.followees(User).count
  json.total_followers user.followers(User).count
  json.following @sender.follows?(user)
end

json.projects(@projects) do |project|
  json.extract! project, :id, :name, :description, :user ,:user_id, :scenes ,:video_url, :image_url, :audio_url, :created_at, :category
  json.url project_url(project, format: :json)
  json.total_comments project.comments.count
  json.total_likes project.likers(User).count
  json.server_time Time.zone.now
  json.liked project.liked_by?(@sender)
  json.scenes project.scenes.as_json(include: { upload: { include: { user: { only: :avatar_url } } } } )
 end
