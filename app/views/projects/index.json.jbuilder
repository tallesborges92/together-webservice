json.array!(@projects) do |project|
  json.extract! project, :id, :name, :description, :user_id, :user, :video_url, :image_url, :audio_url, :created_at, :category, :description_video_url
  json.url project_url(project, format: :json)
  json.total_comments project.comments.count
  json.total_likes project.likers(User).count
  json.server_time Time.zone.now
  json.liked project.liked_by?(@sender)
  json.scenes project.scenes.as_json(include: { upload: { include: { user: { only: :avatar_url } } } } )
end
