json.array!(@likes) do |like|
  json.extract! like, :id, :project_id, :user_id, :user, :project
  json.url like_url(like, format: :json)
end
