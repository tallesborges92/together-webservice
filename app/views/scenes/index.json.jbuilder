json.array!(@scenes) do |scene|
  json.extract! scene, :id, :name, :description, :project_id, :image_url, :time, :start, :upload, :upload_id
  json.url scene_url(scene, format: :json)
end
