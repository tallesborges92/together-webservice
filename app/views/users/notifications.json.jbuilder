json.array!(@notifications) do |notification|
  json.extract! notification, :id, :user_id, :sender_id, :user, :sender, :message, :created_at
  json.url notification_url(notification, format: :json)
  json.server_time Time.zone.now
end
