json.extract! @user, :id, :facebook_id, :name, :email, :avatar_url, :password, :created_at, :updated_at, :cover_url
json.total_followed_users @user.followees(User).count
json.total_followers @user.followers(User).count
json.following @sender.follows?(@user)
