json.array!(@users) do |user|
  json.extract! user, :id, :facebook_id, :name, :email, :avatar_url, :password
  json.url user_url(user, format: :json)
  json.total_followed_users user.followees(User).count
  json.total_followers user.followers(User).count
  json.following @sender.follows?(user)
end
