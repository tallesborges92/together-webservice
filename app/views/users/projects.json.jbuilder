json.array!(@projects) do |project|
  json.extract! project, :id, :name, :description, :user_id, :user, :video_url, :image_url, :audio_url
  json.url project_url(project, format: :json)
  json.total_comments project.comments.count
  json.total_likes project.likers(User).count
  json.liked project.liked_by?(@user)
end
