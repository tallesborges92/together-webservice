json.array!(@texts) do |text|
  json.extract! text, :id, :project_id, :time, :start, :text
  json.url text_url(text, format: :json)
end
