json.array!(@uploads) do |upload|
  json.extract! upload, :id, :user_id, :scene_id, :video_url, :image_url
  json.url upload_url(upload, format: :json)
end
