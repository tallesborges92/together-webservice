json.array!(@comments) do |comment|
  json.extract! comment, :id, :project_id, :user_id, :text, :user, :created_at
  json.url comment_url(comment, format: :json)
  json.server_time Time.zone.now
end
