json.array!(@storyboards) do |storyboard|
  json.extract! storyboard, :id, :image_url
  json.url storyboard_url(storyboard, format: :json)
end
