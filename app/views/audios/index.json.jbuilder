json.array!(@audios) do |audio|
  json.extract! audio, :id, :project_id, :time, :start, :text
  json.url audio_url(audio, format: :json)
end
