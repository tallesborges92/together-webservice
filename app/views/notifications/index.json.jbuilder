json.array!(@notifications) do |notification|
  json.extract! notification, :id, :user_id, :user, :sender_id, :sender, :project, :message, :created_at
  json.url notification_url(notification, format: :json)
  json.server_time Time.zone.now
  json.project notification.project.as_json(include: [:user,:category])
end
