class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session
  before_action :authenticate

  private
    def authenticate
      #head :unauthorized unless params[:token] == 'm0fbzWJniz91Q4Ej3-TnuA'
    end

end
