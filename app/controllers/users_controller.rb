class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy, :follow_user, :like_project, :notifications]
  before_action :set_users, only: [:followed_users, :followers, :projects, :feed, :profile, :projectsUnfinisheds, :participedProjects, :likedProjects]


  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end
 
  # GET /users/1/1
  def profile
    render :profile
  end

  # GET /users/:user/projectsUnfinisheds/:sender
  def projectsUnfinisheds
    @projects = @user.projects.where(video_url: ""). order(updated_at: :desc).page(params[:page])
    render "projects/index"
  end

  # GET /users/1/likedProjects/2
  def likedProjects
    @projects = @user.likeables_relation(Project).page(params[:page])    
    render "projects/index"
  end

  # GET /users/:user/participedProjects/:sender
  def participedProjects
    #@projects = @user.projects.order(updated_at: :desc)
    @projects = Project.joins(scenes: :uploads).where(uploads: { user_id: @user.id} ).group(:project_id).order(created_at: :desc).page(params[:page])
    render 'projects/index'
  end
  # GET /users/1/feed
  def feed
    @projects = Project.all.order(updated_at: :desc).page(params[:page])
    render "projects/index"    
  end

  # GET /users

  # GET /users/1/notifications.json
  def notifications
    @notifications = @user.notifications.order(created_at: :desc).page(params[:page])
    render "notifications/index"
  end

  # GET /users/1/followed_users
  def followed_users
    @users = @user.followees(User)
    render "users/index"
  end

    # GET /users/1/followers
  def followers
    @users = @user.followers(User)
    render "users/index"
  end

  # GET /users/1/projects
  def projects
    @projects = @user.projects.order(updated_at: :desc).page(params[:page])
    render "projects/index"
  end
 
  # POST /users/1/like_project
  def like_project
    @project = Project.find(params[:project_id])
    
    if @user.like!(@project)
      Notification.create!(:user_id => @project.user.id, :sender_id => @user.id, :project_id => @project.id ,  :message => 'like_project')
      data = { :alert => @user.name + ' like the project ' + @project.name, :sound => 'default' }
      push = Parse::Push.new(data, 'user' + @project.user.id.to_s)
      push.type = "ios"
      push.save
      render json: { :code => 1, :message => "like_success" }
    else
      @user.unlike!(@project)
      render json: { :code => 1, :message => "unlike_success" }
    end
  end

  # POST /users/1/follow_user
  def follow_user
    @followable_user = User.find(params[:user][:followable_id])

    if @user.follow!(@followable_user)
      Notification.create!(:user_id => @followable_user.id, :sender_id => @user.id, :message => 'follow')
      data = { :alert => @user.name + ' start following you', :sound => 'default'}
      push = Parse::Push.new(data, 'user' + @project.user.id.to_s)
      push.type = "ios"
      push.save
      render json: { :code => 1, :message => "follow_success" }       
    else
      @user.unfollow!(@followable_user)
      render json: { :code => 0, :message => "follow_error" }
    end

  end
  
  # POST /users/login.json
  def login
    @user = User.where(email: params[:user][:email], password: params[:user][:password]).take
    if @user
      render :show, status: :created, location: @user
    else
      render json: '0'
    end
  end

  # POST /users
  # POST /users.json
  def create 
    facebook_id = params[:user][:facebook_id]

    if facebook_id.empty?
      @user = User.new(user_params)

      respond_to do |format|
        if @user.save
          format.html { redirect_to @user, notice: 'User was successfully created.' }
          format.json { render :show, status: :created, location: @user }
        else
          format.html { render :new }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      end
    else
      @user = User.where(facebook_id: params[:user][:facebook_id]).take
      if @user
        render :show, status: :created, location: @user
      else
        @user = User.new(user_params)

        respond_to do |format|
          if @user.save
            format.html { redirect_to @user, notice: 'User was successfully created.' }
            format.json { render :show, status: :created, location: @user }
          else
            format.html { render :new }
            format.json { render json: @user.errors, status: :unprocessable_entity }
          end
        end
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    def set_users
      @user = User.find(params[:id])
      @sender = params[:sender].nil? ? @user : User.find(params[:sender])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:facebook_id, :name, :email, :avatar_url, :password,:project_id, :cover_url)
    end
end
