class SearchesController < ApplicationController

  def search
    @sender = User.find(params[:sender])
    wildcard_search = "%#{params[:param]}%"

    @users = User.where('name LIKE ?', wildcard_search).limit(5)
    @projects = Project.where('name LIKE ?', wildcard_search).order(created_at: :desc).limit(5)
  end

end
