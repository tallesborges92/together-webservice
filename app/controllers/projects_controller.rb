class ProjectsController < ApplicationController
  before_action :set_project, only: [:show, :edit, :update, :destroy, :scenes, :texts, :audios, :comments, :likes]

  # GET /projects
  # GET /projects.json
  def index
    @projects = Project.where.not(video_url: "").order(updated_at: :desc)
  end

  # GET /projects/1
  # GET /projects/1.json
  def show
  end

  # GET /projects/new
  def new
    @project = Project.new
  end

  # GET /projects/1/edit
  def edit
  end

  # GET /projects/1/scenes
  def scenes
    @scenes = @project.scenes
  end

  # GET /projects/1/audios
  def audios
    @audios = @project.audios
  end

  # GET /projects/1/texts
  def texts
    @texts = @project.texts
  end

  # GET /projects/1/comments
  def comments
    @comments = @project.comments
    render "comments/index"
  end
  # GET /projects/1/likes
  def likes
    @user = User.find(params[:user_id])
    @sender = @user
    @users = @project.likers(User)
    render "users/index"
  end

  # POST /projects
  # POST /projects.json
  def create
    @project = Project.new(project_params)

    respond_to do |format|
      if @project.save
        format.html { redirect_to @project, notice: 'Project was successfully created.' }
        format.json { render :show, status: :created, location: @project }
      else
        format.html { render :new }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /projects/1
  # PATCH/PUT /projects/1.json
  def update
    respond_to do |format|
      if @project.update(project_params)
        format.html { redirect_to @project, notice: 'Project was successfully updated.' }
        format.json { render :show, status: :ok, location: @project }
      else
        format.html { render :edit }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /projects/1
  # DELETE /projects/1.json
  def destroy
    @project.destroy
    respond_to do |format|
      format.html { redirect_to projects_url, notice: 'Project was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_project
      @project = Project.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def project_params
      params.require(:project).permit(:name, :description, :user_id, :video_url, :image_url, :audio_url, :category_id, :description_video_url)
    end
end
