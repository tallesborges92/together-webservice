class Project < ActiveRecord::Base
  belongs_to :user
  has_many :scenes
  has_many :texts
  has_many :audios
  has_many :comments
  has_many :notifications, :dependent => :delete_all
  belongs_to :category
  acts_as_likeable
end
