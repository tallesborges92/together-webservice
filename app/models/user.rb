class User < ActiveRecord::Base
	has_many :projects
	has_many :notifications, :dependent => :delete_all
	acts_as_follower
	acts_as_followable
	acts_as_liker
end
