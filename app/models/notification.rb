class Notification < ActiveRecord::Base
  paginates_per 20
  belongs_to :user, :class_name => 'User'
  belongs_to :sender, :class_name => 'User'
  belongs_to :project
end
