class Scene < ActiveRecord::Base
  belongs_to :project
  has_many :uploads
  belongs_to :upload
end
