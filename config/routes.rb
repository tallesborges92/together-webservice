Rails.application.routes.draw do
  resources :storyboards

  resources :notifications

  resources :categories
  get 'categories/:id/projects/:type/:sender' => 'categories#projects'

  resources :comments

  resources :audios

  resources :texts

  resources :uploads
 
  resources :scenes
  get 'scenes/:id/uploads' => 'scenes#uploads'

  resources :projects
  get 'projects/:id/scenes' => 'projects#scenes'
  get 'projects/:id/texts' => 'projects#texts'
  get 'projects/:id/audios' => 'projects#audios'
  get 'projects/:id/comments' => 'projects#comments'
  get 'projects/:id/likes' => 'projects#likes'

  resources :users
  get 'users/:id/likedProjects/:sender' => 'users#likedProjects'
  get 'users/:id/participedProjects/:sender' => 'users#participedProjects'
  get 'users/:id/projectsUnfinisheds/:sender' => 'users#projectsUnfinisheds'
  get 'users/:id/projects/:sender' => 'users#projects'
  get 'users/:id/followed_users/:sender' => 'users#followed_users'
  get 'users/:id/followers/:sender' => 'users#followers'
  post 'users/login' => 'users#login'
  post 'users/:id/follow_user' => 'users#follow_user'
  post 'users/:id/like_project' => 'users#like_project'
  get 'users/:id/notifications' => 'users#notifications'
  get 'users/:id/feed' => 'users#feed'
  get 'users/:id/:sender' => 'users#profile'

  post 'searches/:sender' => 'searches#search'


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
