class CreateUploads < ActiveRecord::Migration
  def change
    create_table :uploads do |t|
      t.references :user, index: true
      t.references :scene, index: true
      t.string :video_url
      t.string :image_url

      t.timestamps
    end
  end
end
