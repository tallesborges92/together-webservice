class CreateStoryboards < ActiveRecord::Migration
  def change
    create_table :storyboards do |t|
      t.string :image_url

      t.timestamps
    end
  end
end
