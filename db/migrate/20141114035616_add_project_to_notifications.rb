class AddProjectToNotifications < ActiveRecord::Migration
  def change
    add_reference :notifications, :project, index: true
  end
end
