class AddUploadToScenes < ActiveRecord::Migration
  def change
    add_reference :scenes, :upload, index: true
  end
end
