class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :facebook_id
      t.string :name
      t.string :email
      t.string :avatar_url
      t.string :password

      t.timestamps
    end
  end
end
