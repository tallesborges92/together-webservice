class CreateAudios < ActiveRecord::Migration
  def change
    create_table :audios do |t|
      t.references :project, index: true
      t.float :time
      t.float :start
      t.string :text

      t.timestamps
    end
  end
end
