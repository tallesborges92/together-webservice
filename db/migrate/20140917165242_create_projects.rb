class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :name
      t.string :description
      t.references :user, index: true
      t.string :video_url
      t.string :image_url
      t.string :audio_url

      t.timestamps
    end
  end
end
