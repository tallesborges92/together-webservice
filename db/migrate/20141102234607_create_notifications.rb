class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.references :user, index: true
      t.references :sender, index: true
      t.string :message

      t.timestamps
    end
  end
end
