class CreateScenes < ActiveRecord::Migration
  def change
    create_table :scenes do |t|
      t.string :name
      t.string :description
      t.references :project, index: true
      t.string :image_url
      t.float :time
      t.float :start

      t.timestamps
    end
  end
end
