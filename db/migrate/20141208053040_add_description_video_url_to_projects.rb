class AddDescriptionVideoUrlToProjects < ActiveRecord::Migration
  def change
    add_column :projects, :description_video_url, :string
  end
end
