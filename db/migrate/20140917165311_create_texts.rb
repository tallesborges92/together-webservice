class CreateTexts < ActiveRecord::Migration
  def change
    create_table :texts do |t|
      t.references :project, index: true
      t.float :time
      t.float :start
      t.string :text

      t.timestamps
    end
  end
end
