# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141208053040) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "audios", force: true do |t|
    t.integer  "project_id"
    t.float    "time"
    t.float    "start"
    t.string   "text"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "audios", ["project_id"], name: "index_audios_on_project_id", using: :btree

  create_table "categories", force: true do |t|
    t.string   "name"
    t.string   "image_url"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "comments", force: true do |t|
    t.integer  "project_id"
    t.integer  "user_id"
    t.string   "text"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "comments", ["project_id"], name: "index_comments_on_project_id", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "follows", force: true do |t|
    t.string   "follower_type"
    t.integer  "follower_id"
    t.string   "followable_type"
    t.integer  "followable_id"
    t.datetime "created_at"
  end

  add_index "follows", ["followable_id", "followable_type"], name: "fk_followables", using: :btree
  add_index "follows", ["follower_id", "follower_type"], name: "fk_follows", using: :btree

  create_table "likes", force: true do |t|
    t.string   "liker_type"
    t.integer  "liker_id"
    t.string   "likeable_type"
    t.integer  "likeable_id"
    t.datetime "created_at"
  end

  add_index "likes", ["likeable_id", "likeable_type"], name: "fk_likeables", using: :btree
  add_index "likes", ["liker_id", "liker_type"], name: "fk_likes", using: :btree

  create_table "mentions", force: true do |t|
    t.string   "mentioner_type"
    t.integer  "mentioner_id"
    t.string   "mentionable_type"
    t.integer  "mentionable_id"
    t.datetime "created_at"
  end

  add_index "mentions", ["mentionable_id", "mentionable_type"], name: "fk_mentionables", using: :btree
  add_index "mentions", ["mentioner_id", "mentioner_type"], name: "fk_mentions", using: :btree

  create_table "notifications", force: true do |t|
    t.integer  "user_id"
    t.integer  "sender_id"
    t.string   "message"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "project_id"
  end

  add_index "notifications", ["project_id"], name: "index_notifications_on_project_id", using: :btree
  add_index "notifications", ["sender_id"], name: "index_notifications_on_sender_id", using: :btree
  add_index "notifications", ["user_id"], name: "index_notifications_on_user_id", using: :btree

  create_table "projects", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "user_id"
    t.string   "video_url"
    t.string   "image_url"
    t.string   "audio_url"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "category_id"
    t.string   "description_video_url"
  end

  add_index "projects", ["category_id"], name: "index_projects_on_category_id", using: :btree
  add_index "projects", ["user_id"], name: "index_projects_on_user_id", using: :btree

  create_table "scenes", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "project_id"
    t.string   "image_url"
    t.float    "time"
    t.float    "start"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "upload_id"
  end

  add_index "scenes", ["project_id"], name: "index_scenes_on_project_id", using: :btree
  add_index "scenes", ["upload_id"], name: "index_scenes_on_upload_id", using: :btree

  create_table "storyboards", force: true do |t|
    t.string   "image_url"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "texts", force: true do |t|
    t.integer  "project_id"
    t.float    "time"
    t.float    "start"
    t.string   "text"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "texts", ["project_id"], name: "index_texts_on_project_id", using: :btree

  create_table "uploads", force: true do |t|
    t.integer  "user_id"
    t.integer  "scene_id"
    t.string   "video_url"
    t.string   "image_url"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "uploads", ["scene_id"], name: "index_uploads_on_scene_id", using: :btree
  add_index "uploads", ["user_id"], name: "index_uploads_on_user_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "facebook_id"
    t.string   "name"
    t.string   "email"
    t.string   "avatar_url"
    t.string   "password"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "cover_url"
  end

end
